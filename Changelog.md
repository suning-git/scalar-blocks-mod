# Version 1.0.1

- Fixed a multithreading bug for even dimensions.  `scalar_blocks`
  should now be helgrind-clean for both even and non-even dimensions.

# Version 1.0.0

- Add support for user specified poles passed as an argument to `--poles`.

- Multiple pole orders are no longer supported.

- If the arguments for `--order` is the same as for `--poles`,
  shifting ends up just being a very complicated way of multiplying by
  one.  So in that case scalar_blocks no longer shifts the poles.

- Fixed a data race.  `scalar_blocks` should now be helgrind-clean.

- Started using proper version numbers and added a `--version` option.

# Jan 16, 2020

- Add support for non-integral dimensions and even integral dimensions.

- Add option to print poles.

- Work around a 32 bit bug.
