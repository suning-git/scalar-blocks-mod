#include <vector>
#include <utility>
#include <cstdint>

std::vector<std::pair<int64_t,int64_t>> mn_deriv_set(const int64_t &nmax)
{
  std::vector<std::pair<int64_t,int64_t>> result;
  for(int64_t m=0; m<2*nmax; ++m)
    {
      result.emplace_back(m,0);
    }
  return result;
}
