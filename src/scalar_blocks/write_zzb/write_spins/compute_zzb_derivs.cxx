#include "../../../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_zzb_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &ab_derivs,
  const int64_t &n_max)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(
    2 * n_max);
  for(size_t j = 0; j < result.size(); ++j)
    {
      // Explicitly specify size_t to handle cases when size_t != uint64_t
      result[j].resize(std::min<size_t>(2 * n_max - j, j + 1), {0});

      for(size_t k = 0; k < result[j].size(); ++k)
        for(size_t n = 0; n <= (j + k) / 2; ++n)
          for(size_t m = 0; m + 2 * n <= j + k; ++m)
            {
              // Boost does not handle negative factorials.  The
              // result is complex infinity, which makes the whole
              // contribution zero.  So we special case them.
              if(j >= m && j + k >= (m + 2 * n) && j <= m + 2 * n)
                {
                  result[j][k]
                    += ab_derivs.at(n).at(j + k - 2 * n) * pow(-1, j - m)
                       * factorial(j) * factorial(k) * factorial(2 * n)
                       / (factorial(j - m) * factorial(m)
                          * factorial(j + k - m - 2 * n) * factorial(n)
                          * factorial(-j + m + 2 * n));
                }
            }
    }
  return result;
}

inline Bigfloat convolve_coefficient(const int64_t &m, const int64_t &n, const int64_t &i, const int64_t &j, const Bigfloat & d, const std::vector<Bigfloat> & pow_cache)
{
	// pow_cache[i+j] = pow(2, i + j - 2*d)
	/*
	return 2 * pow(-1, i + j) * binomial_coefficient(m, i) * binomial_coefficient(n, j) 
		* Pochhammer(d - i + 1, i) * Pochhammer(d - j + 1, j) 
		* pow_cache[i+j];
		*/
	return 2 * ((i + j) % 2 == 0 ? 1 : -1) * binomial_coefficient(m, i) * binomial_coefficient(n, j)
		* Pochhammer(d - i + 1, i) * Pochhammer(d - j + 1, j)
		* pow_cache[i + j];

	//return 2 * pow(-1, i + j) * binomial_coefficient(m, i) * binomial_coefficient(n, j) * Pochhammer(d - i + 1, i) * Pochhammer(d - j + 1, j) * pow(2, i + j - 2*d);
	// 2 (-1)^(i + j) Binomial[m, i] Binomial[n, j] Pochhammer[d - i + 1, i] Pochhammer[d - j + 1, j](1 - 1 / 2) ^ (-i + d) (1 - 1 / 2) ^ (-j + d)
	//return 2 * pow(-1, i + j) * binomial_coefficient(m, i) * binomial_coefficient(n, j) * Pochhammer(d - i + 1, i) * Pochhammer(d - j + 1, j) * pow(0.5, -i + d - j + d);
}

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_convolve_zzb_derivs(
	const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
	&zzb_derivs, const Bigfloat & conv_factor, const Bigfloat & convention_factor)
{
	std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(zzb_derivs.size());

	std::vector<Bigfloat> pow_cache;

	//std::cout << "pdmax = " << pdmax << " \n";

	size_t pdmax = zzb_derivs.size() -1;
	for (size_t i = 0; i <= pdmax; i++) pow_cache.push_back(pow(2, i - 2 * conv_factor));



	for (size_t m = 0; m < zzb_derivs.size(); ++m)
	{
		//std::cout << " --------------- m= " << m << " --------------- \n";

		result[m].resize(zzb_derivs[m].size(), { 0 });
		for (size_t n = 0; n < zzb_derivs[m].size(); ++n)
		{
			//std::cout << "n= " << n << "\n";

			for (size_t i = 0; i <= m; ++i)
				for (size_t j = 0; j <= n; ++j)
				{
					result[m][n] += zzb_derivs.at(std::max<size_t>(m - i, n - j)).at(std::min<size_t>(m - i, n - j)) * convolve_coefficient(m, n, i, j, conv_factor, pow_cache);
				}

			result[m][n] *= convention_factor;
		}

	}
	return result;
}


