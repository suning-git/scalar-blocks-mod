# quick correctness check
/usr/bin/time ./build/scalar_blocks --dim 3 --order 5 --max-derivs 6 --spin-ranges 0-21 --poles 6 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665

# order=5 largest calculation
/usr/bin/time ./build/scalar_blocks --dim 3 --order 5 --max-derivs 22 --spin-ranges 0-88 --poles 40 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665

# Largest calculation
/usr/bin/time ./build/scalar_blocks --dim 3 --order 90 --max-derivs 22 --spin-ranges 0-88 --poles 40 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665

# order=5 moderate benchmark
/usr/bin/time ./build/scalar_blocks --dim 3 --order 5 --max-derivs 10 --spin-ranges 0-50 --poles 14 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665

# order=10 moderate benchmark
/usr/bin/time ./build/scalar_blocks --dim 3 --order 10 --max-derivs 10 --spin-ranges 0-50 --poles 14 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665

# moderate benchmark
/usr/bin/time ./build/scalar_blocks --dim 3 --order 60 --max-derivs 10 --spin-ranges 0-50 --poles 14 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665

# Using ranges
/usr/bin/time ./build/scalar_blocks --dim 3 --order 60 --max-derivs 10 --spin-ranges 0,12-14,22-33,35,44,47-50 --poles 14 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark --debug=true --precision=665


# # perf
# perf record -ag  ./build/scalar_blocks --dim 3 --order 10 --max-derivs 10 --max-spin 50 --poles 14 --delta-12 0.8791  --delta-34 0.8791 --num-threads=4 -o tests/benchmark
# perf script | ../FlameGraph/stackcollapse-perf.pl > out.perf_folded
# ../FlameGraph/flamegraph.pl out.perf_folded > perf.svg


/usr/bin/time ~/install/bin/scalar_blocks --dim 3 --order 60 --max-derivs 6 --spin-ranges 0-21 --poles 8 --delta-12 0.9908  --delta-34 0 --num-threads=4 -o tests/benchmark --debug=true --precision=768

/usr/bin/time /home/wlandry/install/bin/scalar_blocks --dim 3 --order 60 --max-derivs 6 --spin-ranges 0-21 --poles 8 --delta-12 0.9908  --delta-34 0 --num-threads=4 -o tests/benchmark --precision=768



