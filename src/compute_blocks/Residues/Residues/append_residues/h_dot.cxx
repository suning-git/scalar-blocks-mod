#include "../../../Residues.hxx"
#include "../../../dot.hxx"

Bigfloat
h_dot(const std::vector<Bigfloat> &h_infinity, const Residues &residues,
      const Bigfloat &nu, const int64_t &recursion_level, const int64_t &rL,
      const Bigfloat &rDelta, const int64_t &rShift)
{
  std::vector<Bigfloat> alpha_delta(
    residues.alpha.at(rL).at(recursion_level - rShift).size()),
    beta_delta(residues.beta.at(rL).at(recursion_level - rShift)),
    gamma_delta(residues.gamma.at(rL).at(recursion_level - rShift));

  for(int64_t n = 0; n < static_cast<int64_t>(alpha_delta.size()); ++n)
    {
      alpha_delta[n] = 1 / (rDelta - (1 - rL - (n + 1)));
    }
  for(int64_t k = 0; k < static_cast<int64_t>(beta_delta.size()); ++k)
    {
      beta_delta[k] = 1 / (rDelta - (1 + nu - (k + 1)));
    }
  for(int64_t n = 0; n < static_cast<int64_t>(gamma_delta.size()); ++n)
    {
      gamma_delta[n] = 1 / (rDelta - (rL + 2 * nu + 1 - (n + 1)));
    }
  return h_infinity.at(recursion_level - rShift)
         + dot(residues.alpha.at(rL).at(recursion_level - rShift), alpha_delta)
         + dot(residues.beta.at(rL).at(recursion_level - rShift), beta_delta)
         + dot(residues.gamma.at(rL).at(recursion_level - rShift),
               gamma_delta);
}
