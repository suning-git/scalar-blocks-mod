#include "../Bigfloat.hxx"

#include <boost/filesystem.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#include <boost/math/tools/polynomial.hpp>

#include <boost/algorithm/string.hpp>

#include <set>

#include <vector>
#include <string>

#include <El.hpp>

using namespace boost::math;
using namespace boost::math::tools; // for polynomial
using boost::lexical_cast;
using std::string;



inline El::BigFloat abs(const El::BigFloat & a)
{
	return (a > 0) ? a : -a;
}

template <typename T>
string sign_str(T const &x)
{
	return x < 0 ? "-" : "+";
}

template <typename T>
string inner_coefficient(T const &x)
{
	string result(" " + sign_str(x) + " ");
	if (abs(x) != T(1))
		result += lexical_cast<string>(abs(x));
	return result;
}

/*! Output in formula format.
For example: from a polynomial in Boost container storage  [ 10, -6, -4, 3 ]
show as human-friendly formula notation: 3x^3 - 4x^2 - 6x + 10.
*/
template <typename T>
string formula_format(polynomial<T> const &a)
{
	string result;
	if (a.size() == 0)
		result += lexical_cast<string>(T(0));
	else
	{
		// First one is a special case as it may need unary negate.
		unsigned i = a.size() - 1;
		if (a[i] < 0)
			result += "-";
		if (abs(a[i]) != T(1))
			result += lexical_cast<string>(abs(a[i]));

		if (i > 0)
		{
			result += "x";
			if (i > 1)
			{
				result += "^" + lexical_cast<string>(i);
				i--;
				for (; i != 1; i--)
					if (a[i])
						result += inner_coefficient(a[i]) + "x^" + lexical_cast<string>(i);

				if (a[i])
					result += inner_coefficient(a[i]) + "x";
			}
			i--;

			if (a[i])
				result += " " + sign_str(a[i]) + " " + lexical_cast<string>(abs(a[i]));
		}
	}
	return result;
} // string formula_format(polynomial<T> const &a)

template <typename T>
string formula_format(std::vector<T> const &a)
{
	string result;
	if (a.size() == 0)
		result += lexical_cast<string>(T(0));
	else
	{
		// First one is a special case as it may need unary negate.
		unsigned i = a.size() - 1;
		if (a[i] < 0)
			result += "-";
		if (abs(a[i]) != T(1))
			result += lexical_cast<string>(abs(a[i]));

		if (i > 0)
		{
			result += "x";
			if (i > 1)
			{
				result += "^" + lexical_cast<string>(i);
				i--;
				for (; i != 1; i--)
					if (a[i])
						result += inner_coefficient(a[i]) + "x^" + lexical_cast<string>(i);

				if (a[i])
					result += inner_coefficient(a[i]) + "x";
			}
			i--;

			if (a[i])
				result += " " + sign_str(a[i]) + " " + lexical_cast<string>(abs(a[i]));
		}
	}
	return result;
} // string formula_format(polynomial<T> const &a)

namespace boost {
	namespace serialization {

		template<class Archive>
		void save(Archive& ar, Bigfloat const& f, const boost::serialization::version_type&) {

			std::ostringstream os;
			//os.precision(mpf_get_prec(f.backend().data()));
			os << f;
			std::string s = os.str();
			//std::cout << s << " has prec=" << mpf_get_prec(f.backend().data()) << "\n";
			ar & s;
		}
		
		template<class Archive>
		void load(Archive& ar, Bigfloat & f, const boost::serialization::version_type&) {
			std::string s;
			ar & s;
			f = Bigfloat(s.c_str());
		}


		template<class Archive>
		void save(Archive& ar, boost::math::tools::polynomial<Bigfloat> const & poly, const boost::serialization::version_type&) {
			ar & poly.data();
		}

		template<class Archive>
		void load(Archive& ar, boost::math::tools::polynomial<Bigfloat> & poly , const boost::serialization::version_type&) {
			ar & poly.data();
		}


		template<class Archive>
		void save(Archive& ar, El::BigFloat const & f, const boost::serialization::version_type&) {
			std::vector<uint8_t> local_array(f.SerializedSize());
			f.Serialize(local_array.data());
			ar & local_array;
		}

		template<class Archive>
		void load(Archive& ar, El::BigFloat & f, const boost::serialization::version_type&) {
			std::vector<uint8_t> local_array(f.SerializedSize());
			ar & local_array;
			f.Deserialize(local_array.data());
		}

	}  // namespace serialization
}  // namespace boost

BOOST_SERIALIZATION_SPLIT_FREE(Bigfloat)
BOOST_SERIALIZATION_SPLIT_FREE(El::BigFloat)
BOOST_SERIALIZATION_SPLIT_FREE(boost::math::tools::polynomial<Bigfloat>)

/*
namespace boost {
	namespace serialization { // insert this code to the appropriate namespaces


		template <typename Archive>
		void save(Archive& ar, ::boost::multiprecision::backends::mpfr_float_backend<0> const& r, unsigned)
		{
			std::string tmp = r.str(0, std::ios::fixed);// 0 indicates use full precision
			ar & tmp;
		}

		template <typename Archive>
		void load(Archive& ar, ::boost::multiprecision::backends::mpfr_float_backend<0>& r, unsigned )
		{
			std::string tmp;
			ar & tmp;
			r = tmp.c_str();
		}

	}
} // re: namespaces
*/


static auto const boost_archive_flags = boost::archive::no_header | boost::archive::no_tracking;

void test_save()
{
	std::vector<El::BigFloat> p1;

	for (int i = 1; i <= 2; i++)
	{
		p1.emplace_back("1.32");
	}

	El::BigFloat v1 = { El::BigFloat("1.3") };

	/**/
	{
		boost::filesystem::ofstream ofs("test.txt");
		boost::archive::binary_oarchive oa(ofs, boost_archive_flags);
		oa & p1;
	}

	std::cout << "test_save end \n";

	std::vector<El::BigFloat> p2;
	El::BigFloat v2;

	{
		boost::filesystem::ifstream ifs("test.txt");
		boost::archive::binary_iarchive ia(ifs, boost_archive_flags);
		ia & p2;
	}

	std::cout << "test_load : p2=" << formula_format(p2) << " \n";





	boost::multiprecision::mpf_float mpf_v("0.2");
	boost::multiprecision::mpfr_float mpfr_v;
	mpfr_v = mpf_v;

	std::cout << "mpf_v = " << mpf_v << " \n";
	std::cout << "mpfr_v = " << mpfr_v << " \n";

	mpf_class mpf_c = mpf_class(mpf_v.backend().data());

	El::BigFloat El_f;
	El_f.gmp_float = mpf_class(mpf_v.backend().data());

	std::cout << "mpf_get_prec(mpf_v) = " << mpf_get_prec(mpf_v.backend().data()) << " ; mpf_get_default_prec=" << mpf_get_default_prec()
		<< " ; mpf_float::default_precision() = " << boost::multiprecision::mpf_float::default_precision() << "\n";

	std::cout << "_mp_size = " << El_f.gmp_float.get_mpf_t()[0]._mp_size << " ; sizeof(mp_exp_t) = " << sizeof(mp_exp_t) << "; sizeof(mp_limb_t)=" << sizeof(mp_limb_t)
		<< " ; sizeof(int)=" << sizeof(int) << " ; El::gmp::num_limbs=" << El::gmp::num_limbs << "\n";

	std::cout.precision(50);
	std::cout << std::fixed;
	std::cout << "test El_f = " << El_f << " ; prec = " << El_f.Precision() << "; data_size=" << El_f.SerializedSize() << "\n";
}


void test_save2()
{
	boost::math::tools::polynomial<Bigfloat> p1 = { 1, 2, 3.4, 5.6 };

	{
		boost::filesystem::ofstream ofs("test.txt");
		boost::archive::text_oarchive oa(ofs);
		oa & p1;
	}

	std::cout << "test_save end \n";

	boost::math::tools::polynomial<Bigfloat> p2;

	{
		boost::filesystem::ifstream ifs("test.txt");
		boost::archive::text_iarchive ia(ifs);
		ia & p2;
	}

	std::cout << "test_load : p2=" << formula_format(p2) << " \n";


	//std::vector<Bigfloat> v1 = { 1, 2, 3.4, 5.6 };
	//std::vector<Bigfloatr> v2;
	//v2.assign(std::begin(v1), std::end(v1));

	boost::math::tools::polynomial<Bigfloatr> p3;
	p3.data().assign(std::begin(p1.data()), std::end(p1.data()));

	std::cout << "test : p3=" << formula_format(p3) << " \n";


	// test precision and memory

	boost::multiprecision::mpf_float mpf_v(0.2);
	boost::multiprecision::mpfr_float mpfr_v;
	mpfr_v = mpf_v;

	std::cout << "mpf_v = " << mpf_v << " \n";
	std::cout << "mpfr_v = " << mpfr_v << " \n";

	mpf_class mpf_c = mpf_class(mpf_v.backend().data());

	El::BigFloat El_f;
	El_f.gmp_float = mpf_class(mpf_v.backend().data());

	std::cout << "mpf_get_prec(mpf_v) = " << mpf_get_prec(mpf_v.backend().data()) << " ; mpf_get_default_prec=" << mpf_get_default_prec()
		<< " ; mpf_float::default_precision() = " << boost::multiprecision::mpf_float::default_precision() << "\n";

	std::cout << "_mp_size = " << El_f.gmp_float.get_mpf_t()[0]._mp_size << " ; sizeof(mp_exp_t) = " << sizeof(mp_exp_t) << "; sizeof(mp_limb_t)=" << sizeof(mp_limb_t)
		<< " ; sizeof(int)=" << sizeof(int) << " ; El::gmp::num_limbs=" << El::gmp::num_limbs << "\n";

	std::cout.precision(50);
	std::cout << std::fixed;
	std::cout << "test El_f = " << El_f << " ; prec = " << El_f.Precision() << "; data_size=" << El_f.SerializedSize() << "\n";


	return;
}



void save_binary_convolved_block(boost::filesystem::ofstream & ofs,
	const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> & zzb_derivs_conv, const size_t binary_out_precision)
{
	std::vector<std::vector<std::vector<El::BigFloat>>> zzb_derivs_conv_El(zzb_derivs_conv.size());
	for (size_t m = 0; m < zzb_derivs_conv.size(); ++m)
	{
		zzb_derivs_conv_El[m].resize(zzb_derivs_conv[m].size());
		for (size_t n = 0; n < zzb_derivs_conv[m].size(); ++n)
		{
			for (size_t k = 0; k < zzb_derivs_conv[m][n].size(); ++k)
			{
				El::BigFloat El_f;
				El_f.gmp_float = mpf_class(zzb_derivs_conv[m][n][k].backend().data());
				El_f.SetPrecision(binary_out_precision);
				zzb_derivs_conv_El[m][n].emplace_back(El_f);
			}
		}
	}
	boost::archive::binary_oarchive oa(ofs, boost_archive_flags);
	oa & zzb_derivs_conv_El;
	return;
}









