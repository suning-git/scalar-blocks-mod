(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     10336,        297]
NotebookOptionsPosition[      9439,        268]
NotebookOutlinePosition[      9796,        284]
CellTagsIndexPosition[      9753,        281]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"r", "[", 
    RowBox[{"z_", ",", "zb_"}], "]"}], ":=", 
   FractionBox[
    RowBox[{
     SqrtBox["z"], " ", 
     SqrtBox["zb"]}], 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "+", 
       SqrtBox[
        RowBox[{"1", "-", "z"}]]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{"1", "+", 
       SqrtBox[
        RowBox[{"1", "-", "zb"}]]}], ")"}]}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"c", "[", 
    RowBox[{"z_", ",", "zb_"}], "]"}], ":=", 
   FractionBox[
    RowBox[{"1", "-", 
     RowBox[{
      SqrtBox[
       RowBox[{"1", "-", "z"}]], " ", 
      SqrtBox[
       RowBox[{"1", "-", "zb"}]]}]}], 
    RowBox[{
     SqrtBox["z"], " ", 
     SqrtBox["zb"]}]]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"one", "=", 
   RowBox[{"SetPrecision", "[", 
    RowBox[{"1", ",", "100"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"half", "=", 
   RowBox[{"one", "/", "2"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.751312630557961*^9, 3.7513126596896753`*^9}, {
  3.75131269400921*^9, 3.7513127628552933`*^9}, {3.751312879965992*^9, 
  3.751312920132419*^9}, {3.7513129561153717`*^9, 3.7513129574600973`*^9}, {
  3.751313926466905*^9, 3.7513139582970953`*^9}, {3.751314580237974*^9, 
  3.751314629844977*^9}, {3.751314870765129*^9, 
  3.751314871101638*^9}},ExpressionUUID->"6b01648c-9c80-4b18-85bb-\
e0cab9aa7f02"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"rcTozzbDerivTable", "[", "derivMax_", "]"}], ":=", 
   "\[IndentingNewLine]", 
   RowBox[{"Flatten", "[", 
    RowBox[{
     RowBox[{"Table", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"zzbDeriv", "[", 
          RowBox[{"p", ",", "q"}], "]"}], "\[Rule]", 
         RowBox[{"Expand", "[", 
          RowBox[{
           RowBox[{"D", "[", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"f", "[", 
              RowBox[{
               RowBox[{"r", "[", 
                RowBox[{"z", ",", "zb"}], "]"}], ",", 
               RowBox[{"c", "[", 
                RowBox[{"z", ",", "zb"}], "]"}]}], "]"}], ",", 
             "\[IndentingNewLine]", 
             RowBox[{"{", 
              RowBox[{"z", ",", "p"}], "}"}], ",", "\[IndentingNewLine]", 
             RowBox[{"{", 
              RowBox[{"zb", ",", "q"}], "}"}]}], "\[IndentingNewLine]", "]"}],
            "/.", 
           RowBox[{"{", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"z", "\[Rule]", "half"}], ",", 
             RowBox[{"zb", "\[Rule]", "half"}]}], "\[IndentingNewLine]", 
            "}"}]}], "]"}]}], "/.", 
        RowBox[{"{", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"Derivative", "[", 
              RowBox[{"m_", ",", "n_"}], "]"}], "[", "f", "]"}], "[", 
            RowBox[{"_", ",", "_"}], "]"}], "\[RuleDelayed]", 
           RowBox[{"rcDeriv", "[", 
            RowBox[{"m", ",", "n"}], "]"}]}], ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"f", "[", 
            RowBox[{"_", ",", "_"}], "]"}], "\[RuleDelayed]", 
           RowBox[{"rcDeriv", "[", 
            RowBox[{"0", ",", "0"}], "]"}]}]}], "\[IndentingNewLine]", 
         "}"}]}], ",", "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{"p", ",", "0", ",", "derivMax"}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{"q", ",", "0", ",", 
         RowBox[{"derivMax", "-", "p"}]}], "}"}]}], "\[IndentingNewLine]", 
      "]"}], ",", "1"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7513122283935003`*^9, 3.7513123031331873`*^9}, {
  3.751312356090934*^9, 3.751312360831118*^9}, {3.751312427366807*^9, 
  3.751312441469873*^9}, {3.751312479686203*^9, 3.7513125030698957`*^9}, {
  3.751312783116528*^9, 3.75131282387574*^9}, {3.751312971038103*^9, 
  3.751312977917961*^9}, {3.75131397307554*^9, 3.7513140685535517`*^9}, {
  3.7513140991333857`*^9, 3.751314206942034*^9}, {3.751314293695077*^9, 
  3.7513142981685343`*^9}, {3.751314608873702*^9, 
  3.751314610969831*^9}},ExpressionUUID->"71db9022-5bae-4cc5-a5bd-\
e86719ab72ef"],

Cell[CellGroupData[{

Cell["Testing", "Subchapter",
 CellChangeTimes->{{3.751314951709957*^9, 
  3.75131495263762*^9}},ExpressionUUID->"9cd9b1ee-85bb-4bf2-87d3-\
9f2984cefd8e"],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"myFunctionRc", "[", 
    RowBox[{"r_", ",", "c_"}], "]"}], ":=", 
   RowBox[{
    FractionBox["1", "2"], 
    SqrtBox[
     RowBox[{
      SuperscriptBox["c", "2"], "+", 
      SuperscriptBox["r", "2"], "+", 
      RowBox[{"7", 
       SuperscriptBox["r", "4"]}], "+", 
      RowBox[{"13", "c"}]}]]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"myFunctionZZb", "[", 
    RowBox[{"z_", ",", "zb_"}], "]"}], ":=", 
   RowBox[{"myFunctionRc", "[", 
    RowBox[{
     RowBox[{"r", "[", 
      RowBox[{"z", ",", "zb"}], "]"}], ",", 
     RowBox[{"c", "[", 
      RowBox[{"z", ",", "zb"}], "]"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"myRCDerivs", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{"Table", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"rcDeriv", "[", 
         RowBox[{"m", ",", "n"}], "]"}], "\[Rule]", 
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"myFunctionRc", "[", 
           RowBox[{"r", ",", "c"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"r", ",", "m"}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{"c", ",", "n"}], "}"}]}], "]"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"r", "\[Rule]", 
          RowBox[{"r", "[", 
           RowBox[{"half", ",", "half"}], "]"}]}], ",", 
         RowBox[{"c", "\[Rule]", 
          RowBox[{"c", "[", 
           RowBox[{"half", ",", "half"}], "]"}]}]}], "}"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"m", ",", "0", ",", "3"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"n", ",", "0", ",", 
        RowBox[{"3", "-", "m"}]}], "}"}]}], "\[IndentingNewLine]", "]"}], 
    "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"myZZbDerivs", "=", 
   RowBox[{"Flatten", "[", 
    RowBox[{"Table", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"zzbDeriv", "[", 
         RowBox[{"m", ",", "n"}], "]"}], "\[Rule]", 
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"myFunctionZZb", "[", 
           RowBox[{"z", ",", "zb"}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"z", ",", "m"}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{"zb", ",", "n"}], "}"}]}], "]"}]}], "/.", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"z", "\[Rule]", "half"}], ",", 
         RowBox[{"zb", "\[Rule]", "half"}]}], "}"}]}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"m", ",", "0", ",", "3"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"{", 
       RowBox[{"n", ",", "0", ",", 
        RowBox[{"3", "-", "m"}]}], "}"}]}], "\[IndentingNewLine]", "]"}], 
    "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.7513141883674707`*^9, 3.751314227116604*^9}, {
  3.7513144692955513`*^9, 3.751314654061324*^9}, {3.751314884749342*^9, 
  3.751315023481662*^9}},ExpressionUUID->"cf6b1089-7149-446d-a2fb-\
04f8c5028d59"],

Cell[CellGroupData[{

Cell[BoxData[
 FractionBox[
  RowBox[{"Last", "/@", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"rcTozzbDerivTable", "[", "3", "]"}], "/.", "myRCDerivs"}], 
    ")"}]}], 
  RowBox[{"Last", "/@", "myZZbDerivs"}]]], "Input",
 CellChangeTimes->{{3.751314699211934*^9, 3.751314734403471*^9}, {
  3.751314777543701*^9, 3.7513148067184772`*^9}, {3.751314854182255*^9, 
  3.7513148647421417`*^9}},ExpressionUUID->"1baefdb5-32b3-4b4f-a09e-\
844052e4e7d1"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1.`99.66840798696339", ",", 
   "1.000000000000000000000000000000000000000000000000000000000000000000000000\
0000000000000000000000000000000000000000032`96.86870674624649", ",", 
   "1.`98.59421544257046", ",", 
   "1.000000000000000000000000000000000000000000000000000000000000000000000000\
0000000000000000000000000000000000000000001`98.2252932677235", ",", 
   "1.`96.86870674624649", ",", "1.`98.78157196244247", ",", 
   "1.000000000000000000000000000000000000000000000000000000000000000000000000\
0000000000000000000000000000000000000000001`98.1877480979564", ",", 
   "1.`98.59421544257046", ",", "1.`98.1554770817968", ",", 
   "1.`98.2252932677235"}], "}"}]], "Output",
 CellChangeTimes->{{3.75131470405977*^9, 3.751314735255014*^9}, {
   3.751314782727047*^9, 3.751314827309251*^9}, {3.751314865220064*^9, 
   3.751314978071278*^9}, 
   3.751315025599172*^9},ExpressionUUID->"05635008-d8ab-492a-95a0-\
c4b342f086d9"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1090, 1112},
WindowMargins->{{351, Automatic}, {0, Automatic}},
FrontEndVersion->"11.1 for Mac OS X x86 (32-bit, 64-bit Kernel) (April 27, \
2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1430, 45, 166, "Input", "ExpressionUUID" -> \
"6b01648c-9c80-4b18-85bb-e0cab9aa7f02"],
Cell[1991, 67, 2763, 65, 327, "Input", "ExpressionUUID" -> \
"71db9022-5bae-4cc5-a5bd-e86719ab72ef"],
Cell[CellGroupData[{
Cell[4779, 136, 154, 3, 63, "Subchapter", "ExpressionUUID" -> \
"9cd9b1ee-85bb-4bf2-87d3-9f2984cefd8e"],
Cell[4936, 141, 3031, 88, 281, "Input", "ExpressionUUID" -> \
"cf6b1089-7149-446d-a2fb-04f8c5028d59"],
Cell[CellGroupData[{
Cell[7992, 233, 447, 11, 53, "Input", "ExpressionUUID" -> \
"1baefdb5-32b3-4b4f-a09e-844052e4e7d1"],
Cell[8442, 246, 969, 18, 222, "Output", "ExpressionUUID" -> \
"05635008-d8ab-492a-95a0-c4b342f086d9"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

