#include "../../Nu.hxx"

#include <vector>

std::vector<std::vector<Bigfloat>>
cb_poles(const Nu &nu, const int64_t &L, const int64_t &order)
{
  std::vector<std::vector<Bigfloat>> result(2);
  std::vector<Bigfloat> &single_poles(result[0]), &double_poles(result[1]);
  // These formulas come from Section 2.2 in doc/conventions.pdf
  std::function<void(const int64_t &)> int64_fn([&](const int64_t &nu_int) {
    for(int64_t k = 1; k <= order; ++k)
      {
        const int64_t pole(-k - L + 1);

        if(2 * nu_int + 2 * L + 2 * k <= order)
          {
            double_poles.push_back(pole);
          }
        else
          {
            single_poles.push_back(pole);
          }
      }
    for(int k = 1; 2 * k <= order && k <= nu_int - 1; ++k)
      {
        single_poles.push_back(nu_int + 1 - k);
      }
    for(int k = 1; k <= order && k <= 2 * L + 2 * nu_int; ++k)
      {
        if(!(k > L && k < L + 2 * nu_int))
          {
            single_poles.push_back(1 + 2 * nu_int + L - k);
          }
      }
  });
  // These formulas come from Section 2.1 in doc/conventions.pdf
  std::function<void(const Bigfloat &)> Bigfloat_fn(
    [&](const Bigfloat &nu_Bigfloat) {
      for(int64_t k = 1; k <= order; ++k)
        {
          single_poles.push_back(-k - L + 1);
        }
      for(int64_t k = 1; 2 * k <= order; ++k)
        {
          single_poles.push_back(nu_Bigfloat + 1 - k);
        }
      for(int64_t k = 1; k <= std::min(order, L); ++k)
        {
          single_poles.push_back(1 + 2 * nu_Bigfloat + L - k);
        }
    });
  // Fill the results
  nu.visit(int64_fn, Bigfloat_fn);
  if(result[1].empty())
    {
      result.resize(1);
    }
  return result;
}


std::vector<std::vector<Bigfloat>>
cb_poles_nofakepole(const Nu &nu, const int64_t &L, const int64_t &order)
{
	std::vector<std::vector<Bigfloat>> result(2);
	std::vector<Bigfloat> &single_poles(result[0]), &double_poles(result[1]);
	// These formulas come from Section 2.2 in doc/conventions.pdf
	std::function<void(const int64_t &)> int64_fn([&](const int64_t &nu_int) {   // SN : I haven't rewrite this part! now the code doesn't work for 2D, 4D
		for (int64_t k = 2; k <= order; k = k + 2)
		{
			const int64_t pole(-k - L + 1);

			if (2 * nu_int + 2 * L + 2 * k <= order)
			{
				double_poles.push_back(pole);
			}
			else
			{
				single_poles.push_back(pole);
			}
		}
		for (int k = 1; 2 * k <= order && k <= nu_int - 1; ++k)
		{
			single_poles.push_back(nu_int + 1 - k);
		}
		for (int k = 2; k <= order && k <= 2 * L + 2 * nu_int; k = k + 2)
		{
			if (!(k > L && k < L + 2 * nu_int))
			{
				single_poles.push_back(1 + 2 * nu_int + L - k);
			}
		}
	});
	// These formulas come from Section 2.1 in doc/conventions.pdf
	std::function<void(const Bigfloat &)> Bigfloat_fn(
		[&](const Bigfloat &nu_Bigfloat) {
		for (int64_t k = 2; k <= order; k = k + 2)
		{
			single_poles.push_back(-k - L + 1);
		}
		for (int64_t k = 1; 2 * k <= order; ++k)
		{
			single_poles.push_back(nu_Bigfloat + 1 - k);
		}
		for (int64_t k = 2; k <= std::min(order, L); k = k + 2)
		{
			single_poles.push_back(1 + 2 * nu_Bigfloat + L - k);
		}
	});
	// Fill the results
	nu.visit(int64_fn, Bigfloat_fn);
	if (result[1].empty())
	{
		result.resize(1);
	}
	return result;
}