#include "../../../Bigfloat.hxx"

#include <Eigen/LU>

#include <vector>

std::vector<
  std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
shift_poles(
  const std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
    &unprotected_poles,
  const std::vector<std::vector<Bigfloat>> &keep)
{
  using Eigen_Matrix = Eigen::Matrix<Bigfloat, Eigen::Dynamic, Eigen::Dynamic>;
  using Eigen_Vector = Eigen::Matrix<Bigfloat, Eigen::Dynamic, 1>;
  const int64_t size([&]() {
    int64_t sum(0);
    for(size_t index(0); index != keep.size(); ++index)
      {
        sum += (index + 1) * keep[index].size();
      }
    return sum;
  }());
  Eigen_Matrix A(size, size);

  for(int64_t row = 0; row < size; ++row)
    {
      int64_t m(row - (size / 2));
      int64_t column(0);
      for(size_t pole_degree_index(0); pole_degree_index != keep.size();
          ++pole_degree_index)
        {
          for(size_t index(0); index != keep[pole_degree_index].size();
              ++index)
            {
              int64_t prefactor(1);
              for(int64_t offset(0); offset != int64_t(pole_degree_index + 1);
                  ++offset)
                {
                  // TODO: Is this a derivative?
                  A(row, column)
                    = prefactor
                      * pow(keep[pole_degree_index][index], m - offset);
                  ++column;
                  prefactor *= (m - offset);
                }
            }
        }
    }
  Eigen_Vector b(size);
  for(int64_t row = 0; row < size; ++row)
    {
      int64_t m(row - (size / 2));
      b(row) = 0;
      for(size_t pole_degree_index(0);
          pole_degree_index != unprotected_poles.size(); ++pole_degree_index)
        {
          for(auto &pole : unprotected_poles[pole_degree_index])
            {
              int64_t prefactor(1);
              for(int64_t offset(0); offset != int64_t(pole.second.size());
                  ++offset)
                {
                  b(row)
                    += prefactor
                       * pole.second[int64_t(pole.second.size()) - offset - 1]
                       * pow(pole.first, m - offset);
                  prefactor *= (m - offset);
                }
            }
        }
    }

  std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>> result(
    keep.size());

  if(size > 0)
    {
      Eigen_Vector x(A.lu().solve(b));
      size_t row(0);
      for(size_t pole_degree_index(0); pole_degree_index != result.size();
          ++pole_degree_index)
        {
          result[pole_degree_index].reserve(keep[pole_degree_index].size());
          for(int64_t kept_index(0);
              kept_index < int64_t(keep[pole_degree_index].size());
              ++kept_index)
            {
              std::vector<Bigfloat> terms(pole_degree_index + 1);
              for(size_t index(0); index < pole_degree_index + 1; ++index)
                {
                  terms[pole_degree_index - index] = x(row);
                  ++row;
                }
              result[pole_degree_index].emplace_back(
                keep[pole_degree_index][kept_index], terms);
            }
        }
    }
  return result;
}
