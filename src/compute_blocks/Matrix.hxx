#pragma once

#include <vector>
#include <iostream>

template <typename T> struct Matrix
{
  size_t row_size, column_size;
  std::vector<T> data;

  Matrix(const size_t &Row_size, const size_t &Column_size)
      : row_size(Row_size), column_size(Column_size),
        data(row_size * column_size, T(0))
  {}
  T &operator()(const size_t &row, const size_t &column)
  {
    return data.at(column_size*row + column);
  }
  T operator()(const size_t &row, const size_t &column) const
  {
    return data.at(column_size*row + column);
  }
};

template <typename T>
std::ostream & operator<<(std::ostream &os, const Matrix<T> &m)
{
  os << "[";
  for(size_t row=0; row<m.row_size; ++row)
    {
      os << "[";
      for(size_t column=0; column<m.column_size; ++column)
        {
          os << m(row,column);
          if(column!=m.column_size-1)
            {
              os << " ";
            }
        }
      os << "]";
      if(row!=m.row_size-1)
        {
          os << ",\n ";
        }
    }
  os << "]";
  return os;
}
  
