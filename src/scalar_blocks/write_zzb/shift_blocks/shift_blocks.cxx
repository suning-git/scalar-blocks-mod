#include "../cb_poles.hxx"
#include "../Single_Spin.hxx"
#include "../../../Delta_Fraction.hxx"
#include "../../../Timers.hxx"
#include "../../../Nu.hxx"

#include <Eigen/Core>


#include <list>
#include <thread>
#include <limits>
#include <algorithm>

namespace
{
  inline bool
  is_protected(const Bigfloat &p, const int64_t &L, const Bigfloat &nu)
  {
    // 100 seems big enough to account for any rounding errors
    return boost::multiprecision::abs(p - (L == 0 ? -nu : Bigfloat(0)))
           <= std::numeric_limits<Bigfloat>::epsilon() * 100;
  }

  inline bool zeroQ(const Bigfloat &p)
  {
	  // 100 seems big enough to account for any rounding errors
	  return boost::multiprecision::abs(p)
		  <= std::numeric_limits<Bigfloat>::epsilon() * 100;
  }

  inline bool integerQ(const Bigfloat &p)
  {
	  return zeroQ(p - boost::math::round(p));
  }

  inline bool
	  is_fakepole(const Bigfloat &x, const int64_t &L, const Bigfloat &nu)
  {
	  return (integerQ(-L - nu - x / 2) && 2 * (L + nu) + x <= 0) || (integerQ(-x / 2) && x <= 0 && L + x >= 1);
  }

  void add_poles_to_kept(
    const std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
      &poles,
    std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
      &all_kept_poles)
  {
    for(size_t index(0); index != poles.size(); ++index)
      {
        for(auto &pole : poles[index])
          {
            all_kept_poles[index].push_back(pole);
          }
      }
  }
}

Delta_Fraction
shift_fraction(const Delta_Fraction &fraction, const Bigfloat &offset);

std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
shift_poles(
  const std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
    &unprotected_poles,
  const std::vector<std::vector<Bigfloat>> &keep);

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
    &all_kept_poles,
  const boost::math::tools::polynomial<Bigfloat> &polynomial);

std::vector<Single_Spin>
shift_blocks(const std::vector<std::vector<Delta_Fraction>> &blocks,
             const Nu &nu, const int64_t &kept_pole_order,
             const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,
             const bool &need_to_shift_poles, const bool &remove_fake_poleQ, const size_t &num_threads,
             const std::string &timer_prefix, Timers &timers)
{
  const Bigfloat nu_Bigfloat(nu.to_Bigfloat());
  std::vector<Single_Spin> result(blocks.size());

  std::cout << "remove_fake_poleQ=" << remove_fake_poleQ << "\n";

  Eigen::initParallel();
  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      Timer &thread_timer(timers.add_and_start(timer_prefix + "thread_"
                                               + std::to_string(thread_rank)));
      threads.emplace_back(
        [&blocks, &num_threads, &user_poles, &nu, &nu_Bigfloat,
         &kept_pole_order, &need_to_shift_poles, &remove_fake_poleQ,
         &result](const size_t &thread_rank, Timer &timer) {
          for(size_t L = thread_rank; L < blocks.size(); L += num_threads)
            {
              const std::vector<std::vector<Bigfloat>> poles(
                user_poles.empty()
                  ? (remove_fake_poleQ ? cb_poles_nofakepole(nu, L, kept_pole_order) : cb_poles(nu, L, kept_pole_order))
                  : (user_poles.size() > L
                       ? user_poles.at(L)
                       : std::vector<std::vector<Bigfloat>>()));
              result[L].denominators.resize(poles.size());
              std::vector<std::vector<Bigfloat>> keep(poles.size());
              for(size_t index(0); index != poles.size(); ++index)
                {
                  for(auto &pole : poles[index])
                    {
                      Bigfloat pole_to_keep(pole - (L + 2 * nu_Bigfloat));
                      result[L].denominators[index].push_back(pole_to_keep);
                      if(!is_protected(pole_to_keep, L, nu_Bigfloat))
                        {
                          keep[index].push_back(pole_to_keep);
                        }
                    }
                }
              for(auto &fraction : blocks[L])
                {
                  const Delta_Fraction shifted(
                    shift_fraction(fraction, L + 2 * nu_Bigfloat));
                  std::vector<
                    std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>>
                    all_kept_poles(std::max(size_t(2), poles.size())),
                    unprotected_poles(std::max(size_t(2), poles.size()));

                  for(auto &residue : shifted.residues)
                    {
					  if (remove_fake_poleQ && is_fakepole(residue.first, L, nu_Bigfloat)) continue;

                      if(is_protected(residue.first, L, nu_Bigfloat))
                        {
                          all_kept_poles.at(0).push_back(residue);
                        }
                      else
                        {
                          unprotected_poles.at(0).push_back(residue);
                        }
                    }
                  for(auto &residue : shifted.double_residues)
                    {
					  if (remove_fake_poleQ && is_fakepole(residue.first, L, nu_Bigfloat)) continue;

                      if(is_protected(residue.first, L, nu_Bigfloat))
                        {
                          all_kept_poles.at(1).push_back(residue);
                        }
                      else
                        {
                          unprotected_poles.at(1).push_back(residue);
                        }
                    }
                  if(need_to_shift_poles)
                    {
                      add_poles_to_kept(shift_poles(unprotected_poles, keep),
                                        all_kept_poles);
                    }
                  else
                    {
                      add_poles_to_kept(unprotected_poles, all_kept_poles);
                    }
                  result[L].numerator.push_back(
                    together_with_factors(all_kept_poles, shifted.polynomial));
                }
              timer.stop();
            }
        },
        thread_rank, std::ref(thread_timer));
    }
  for(auto &thread : threads)
    {
      thread.join();
    }

  return result;
}
