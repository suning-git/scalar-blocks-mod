#include "Nu.hxx"

Nu::Nu(const std::string &dim_string)
{
  Bigfloat dim;

  try
    {
      dim = Bigfloat(dim_string);
      variant = (dim - 2) / 2;
    }
  catch(const std::runtime_error &e)
    {
      throw std::runtime_error(
        "Expected floating point number for dimension, but found: '"
        + dim_string + "'\n\t Exception from conversion: " + e.what());
    };
  try
    {
      int64_t dim_int(std::stoi(dim_string));
      bool is_integer(dim == Bigfloat(dim_int));
      if(dim_int % 2 == 0 && is_integer)
        {
          variant = (dim_int - 2) / 2;
        }
    }
  // Just ignore errors if it does not parse as an int.
  catch(const std::invalid_argument &e)
    {}
}
