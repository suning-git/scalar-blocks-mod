#include "../../../Residues.hxx"

#include <numeric>

void process_single_spin(const std::vector<Bigfloat> &h_infinity,
                         const int64_t &order, const int64_t &nu,
                         const Bigfloat &Delta_12, const Bigfloat &Delta_34,
                         const int64_t &L_max_recursion,
                         const int64_t &recursion_level, Residues &residues,
                         const int64_t &L);

void append_residues_even_dim(
  const std::vector<Bigfloat> &h_infinity, const int64_t &order,
  const int64_t &nu, const Bigfloat &Delta_12, const Bigfloat &Delta_34,
  const int64_t &L_max_recursion, const int64_t &recursion_level,
  const size_t &num_threads, const size_t &thread_rank,
  const std::vector<uint64_t> &timings, Residues &residues,
  std::vector<uint64_t> &new_timings, Timer &thread_timer)
{
  // The range of spins we actually go over (over all threads) is
  // 0..L_iteration_max-1
  const int64_t L_iteration_max(L_max_recursion - recursion_level + 1);
  if(L_iteration_max > static_cast<int64_t>(timings.size()))
    {
      throw std::runtime_error(
        "INTERNAL ERROR: L_iteration_max > timing.size(): "
        "L_iteration_max: "
        + std::to_string(L_iteration_max)
        + " timings.size(): " + std::to_string(timings.size()));
    }

  // Should think about start_time, end_time and current_time (below) as
  // numerators, with denominator num_threads.
  const uint64_t total_time(std::accumulate(
    timings.begin(), timings.begin() + L_iteration_max, int64_t(0)));
  uint64_t start_time(thread_rank * total_time),
    end_time((thread_rank + 1) * total_time);

  int64_t L_begin(L_iteration_max), L_end(L_iteration_max);
  {
    uint64_t start_interval(0), end_interval(0);
    for(int64_t L = 0; L < L_iteration_max; ++L)
      {
        end_interval += timings[L] * num_threads;
        if(start_time >= start_interval && start_time < end_interval)
          {
            L_begin = L;
          }

        if(end_time >= start_interval && end_time < end_interval)
          {
            L_end = L;
          }
        start_interval = end_interval;
      }
  }

// uint64_t current_time(0);
  // for(int64_t L = 0; L < L_iteration_max; ++L)
  //   {
  //     current_time += timings[L] * num_threads;
  //     if(L_begin == L_iteration_max && current_time >= start_time)
  //       {
  //         L_begin = L;
  //       }
  //     if(L_end == L_iteration_max && current_time >= end_time)
  //       {
  //         L_end = L;
  //       }
  //   }



  for(int64_t L = L_begin; L < L_end; ++L)
    {
      Timer block_timer;

      process_single_spin(h_infinity, order, nu, Delta_12, Delta_34,
                          L_max_recursion, recursion_level, residues, L);

      block_timer.stop();
      new_timings.at(L) = block_timer.elapsed_microseconds();
    }
  thread_timer.stop();
}
