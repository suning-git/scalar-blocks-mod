#include "../Timers.hxx"
#include "../Nu.hxx"
#include "../parse_spin_ranges.hxx"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>

#include <vector>

#include <El.hpp>

namespace po = boost::program_options;

std::vector<std::vector<std::vector<Bigfloat>>>
parse_user_poles(const std::string &user_poles_string);

void test_save();

void write_zzb(
  const boost::filesystem::path &output_dir, const Nu &nu,
  const std::string &dim, const std::string &Delta_12_string,
  const std::string &Delta_34_string,
  const std::pair<std::set<int64_t>, int64_t> &spins, const int64_t &n_max,
  const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,
  const bool &output_ab, const bool &output_poles, 

  const bool &nofakepole, const std::vector<std::string> & convolve_factor_str_list, 
  const bool &nozzbDeriv, const std::string & outfile_suffix, const size_t binary_out_precision,

  const size_t &num_threads, const bool &debug, Timers &timers);

int main(int argc, char *argv[])
{
  El::Environment env(argc, argv);

  try
    {
      po::options_description options("Scalar Blocks options");

      int64_t order, n_max;
      std::string spin_ranges_string;
      size_t num_threads;
      std::string Delta_12, Delta_34, dim, poles_string, outfile_suffix;
      size_t precision_base_2;
      boost::filesystem::path output_directory;
      bool debug(false);
      bool output_ab(false);
      bool output_poles(false);
	  bool nofakepole(false);
	  bool nozzbDeriv(false);
	  size_t binary_out_precision;
	  
	  std::vector<std::string> convolve_factor_str_list;

      options.add_options()("help,h", "Show this helpful message.");
      options.add_options()("version,v", "Show version info.");
      options.add_options()("dim", po::value<std::string>(&dim)->required(),
                            "Dimension");
      options.add_options()("order", po::value<int64_t>(&order)->required(),
                            "Depth of recursion");
      options.add_options()("max-derivs",
                            po::value<int64_t>(&n_max)->required(),
                            "Max number of derivs (n_max)");
      options.add_options()("output-ab", po::bool_switch(&output_ab),
                            "Output a,b derivatvies "
                            "instead of z,zb derivatives.");
      options.add_options()(
        "spin-ranges", po::value<std::string>(&spin_ranges_string)->required(),
        "Comma separated list of ranges of spins to compute "
        "(e.g. 12-16,22,37-49)");
      options.add_options()(
        "delta-12", po::value<std::string>(&Delta_12)->required(), "Δ₁₂");
      options.add_options()(
        "delta-34", po::value<std::string>(&Delta_34)->required(), "Δ₃₄");
      options.add_options()(
        "poles", po::value<std::string>(&poles_string)->required(),
        "Either a single number indicating the pole order to keep, or a set "
        "of user specified poles.  User specified poles are an array of "
        "arrays of arrays with dimensions "
        "[spin (L), degree (single, double, triple, etc.), pole index].  For "
        "example, the input [[[0,-1],[-2.5,-3.5]]] will use single poles for "
        "L=0 at 0 and 1, and double poles at -2.5 and -3.5.  "
        "[[],[[],[],[4.5,5.5]]] "
        "will use no poles for L=0 and triple poles for L=1 at 4.5 and 5.5.");

      options.add_options()("output-poles", po::bool_switch(&output_poles),
                            "Include shifted poles in output");
      options.add_options()("num-threads",
                            po::value<size_t>(&num_threads)->required(),
                            "Number of threads");
      options.add_options()("precision",
                            po::value<size_t>(&precision_base_2)->required(),
                            "The precision, in the number of bits, for "
                            "numbers in the computation.");
      options.add_options()(
        ",o",
        po::value<boost::filesystem::path>(&output_directory)->required(),
        "Output directory");
      options.add_options()("debug",
                            po::value<bool>(&debug)->default_value(false),
                            "Write out debugging output.");

	  options.add_options()(
		  "no-fake-poles", po::bool_switch(&nofakepole),
		  "remove fake pole when Delta12 or Delta34=0.");

	  options.add_options()(
		  "no-zzbDeriv", po::bool_switch(&nozzbDeriv),
		  "remove fake pole when Delta12 or Delta34=0.");

	  options.add_options()(
		  "convolve", po::value< std::vector<std::string> >(&convolve_factor_str_list)->multitoken(),
		  "list of (Δ₂+Δ₃)/2. Example : --convolve A=1.3 B=1.7");

	  options.add_options()(
		  "out-file-suffix", po::value<std::string>(&outfile_suffix),
		  "the output file will be named as [out-file-suffix]-L*.m .");

	  options.add_options()(
		  "binary-out-precision", po::value<size_t>(&binary_out_precision)->default_value(0),
		  "The convolved block will be cast to this precision and save to [convolve_stamp]-L*.block as binary. ");

      po::variables_map variables_map;

      po::store(po::parse_command_line(argc, argv, options), variables_map);
      if(variables_map.count("help"))
        {
          std::cout << options << '\n';
          return 0;
        }
      if(variables_map.count("version"))
        {
          std::cout << "Scalar Blocks " << SCALAR_BLOCKS_VERSION_STRING << '\n';
          return 0;
        }

      po::notify(variables_map);

      // Round up the size to work around bugs in boost::multiprecision
      const size_t precision_base_2_rounded_up(
        (((precision_base_2 - 1) / (sizeof(mp_limb_t) * 8)) + 1)
        * sizeof(mp_limb_t) * 8);
      const size_t precision_base_10(
        boost::multiprecision::detail::digits2_2_10(
          precision_base_2_rounded_up));

	  El::gmp::SetPrecision(precision_base_2_rounded_up);
      boost::multiprecision::mpfr_float::default_precision(precision_base_10);
	  boost::multiprecision::mpf_float::default_precision(precision_base_10);

	  std::cout << "precision_base_10 = " << precision_base_10 << "\n";

      const Bigfloat r_crossing(3 - 2 * sqrt(Bigfloat(2)));

      std::pair<std::set<int64_t>, int64_t> spins(
        parse_spin_ranges(spin_ranges_string));

      Nu nu(dim);

      Timers timers(debug);
      Timer &total_timer(timers.add_and_start("Total"));
      int64_t kept_pole_order([&]() -> int64_t {
        try
          {
            return std::stoi(poles_string);
          }
        catch(...)
          {}
        return std::numeric_limits<int64_t>::max();
      }());
      std::vector<std::vector<std::vector<Bigfloat>>> user_poles(
        parse_user_poles(kept_pole_order != std::numeric_limits<int64_t>::max()
                           ? std::string()
                           : poles_string));

	  if (variables_map.count("convolve"))
	  {
		  //std::cout << "convolve str : " << variables_map["convolve"].as<std::string>() << "\n";
		  //boost::char_separator<char> sep(";");
		  //boost::tokenizer< boost::char_separator<char> > tok(variables_map["convolve"].as<std::string>(), sep);
		  //for (const auto &t : tok) convolve_factor_str_list.push_back(t);

		  std::cout << "convolve factors : \n";
		  for (auto& conv_factor : convolve_factor_str_list) std::cout << conv_factor << "\n";
		  std::cout << "\n";
	  }

      write_zzb(output_directory, nu, dim, Delta_12, Delta_34, spins, n_max,
                kept_pole_order, order, user_poles, output_ab, output_poles, 

				nofakepole, convolve_factor_str_list, nozzbDeriv, outfile_suffix, binary_out_precision,

                num_threads, debug, timers);

	  test_save();

      total_timer.stop();
      if(debug)
        {
          timers.write_profile(output_directory / "profile");
        }
    }
  catch(std::exception &e)
    {
      std::cout << "Error: " << e.what() << "\n";
      exit(1);
    }
  catch(...)
    {
      std::cout << "Unknown error\n";
      exit(1);
    }
}
