# Update goals

The purpose of the update is to

- Allow `--dim=<even-integer>` option values
- While at it, allow `--dim=<non-integer>` values
- Update .md files to give better mathematical and technical details

# Allowing fractional and detecting even integer dimensions

The original version of the code accepts only integer dimensions by using 
`boost`'s option parser. I changed the code so that it asks for `std::string` 
from the option parser, and then parses it into `Dimension` structure. This
structure contains information on whether dimension is integer, even, and 
holds the `Bigfloat` value for `nu`, as well as integer values `nu_int` and `dim_int`
when they are available. In a bunch of places I am now passing the `Dimension`
structure instead of `Bigfloat nu`.

Updated `is_protected` to use a tolerance when comparing poles in fractional dimenions.

# Allowing even dimensions

The main part of the recursion algorithm is hidden in the constructor 
of `Residues`. I have created an overload which takes `int64_t nu` and 
uses the appropriate new code for even dimension. This also required 
adding some new fields into `Residues`. The meaning of the pre-existing
fields in even dimensional case is also different.

`Residues` are only used in `compute_blocks`, where they are constructed and 
then `fill_result` is applied to them. I added `fill_result_even_d` which handles
the case of even dimenions. It produces `Delta_Fraction` which was also updated
to support double poles.

The `Delta_Fraction` that comes out of `fill_result` or `fill_result_even_d` then
goes into `shift_blocks`, which was updated to handle double-poles in a straightforward manner.

The function `cb_poles` was updated to produce an `std::pair` containing a list of
single poles and a list of double poles.

I tested the even-dimensional code against exact results obtained by applying
Dolan-Osborn dimension-raising differential operator to 2-dimensional blocks. I
performed this test for `d=2,4,6,8`. 
