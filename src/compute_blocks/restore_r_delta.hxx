#include "dot.hxx"
#include "../Delta_Fraction.hxx"

std::vector<Delta_Fraction> restore_r_delta(
  const std::vector<Bigfloat> &partial_polynomial,
  const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
    &partial_residues,
  const std::vector<
    std::pair<Bigfloat, std::vector<std::pair<Bigfloat, Bigfloat>>>>
    &partial_double_residues,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta,
  const std::vector<std::pair<int64_t, int64_t>> &derivs);
