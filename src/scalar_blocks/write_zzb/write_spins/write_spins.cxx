#include "../cb_poles.hxx"
#include "../Single_Spin.hxx"
#include "../../../Timer.hxx"
#include "../../../Nu.hxx"

#include <boost/filesystem.hpp>
#include <boost/math/tools/polynomial.hpp>

#include <boost/algorithm/string.hpp>

#include <vector>
#include <set>

std::vector<Bigfloat> a_deriv_coefficients(const int64_t &n);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_ab_derivs(
  const Bigfloat &nu, const Bigfloat &Delta_12, const Bigfloat &Delta_34,
  const int64_t &n_max, const boost::math::tools::polynomial<Bigfloat> &lambda,
  const std::vector<boost::math::tools::polynomial<Bigfloat>> &a_derivs);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_zzb_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &ab_derivs,
  const int64_t &n_max);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
compute_convolve_zzb_derivs(
	const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
	&zzb_derivs, const Bigfloat & conv_factor, const Bigfloat & convention_factor);


void write_derivs(std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> & zzb_derivs, const std::string & tab_name, boost::filesystem::ofstream & outfile)
{
	for (size_t z = 0; z < zzb_derivs.size(); ++z)
	{
		for (size_t zb = 0; zb < zzb_derivs.at(z).size(); ++zb)
		{
			outfile << tab_name << "[" << z << "," << zb << "] -> ";

			//std::cout << tab_name << " : z=" << z << ", zb=" << zb << " : " << zzb_derivs[z][zb] << '\n';

			for (size_t power = 0; power < zzb_derivs.at(z).at(zb).size();
				++power)
			{
				outfile << zzb_derivs.at(z).at(zb)[power];
				if (power != 0)
				{
					outfile << "*x^" << power;
				}
				if (power + 1 != zzb_derivs.at(z).at(zb).size())
				{
					outfile << "\n   + ";
				}
			}
			if (zb + 1 != zzb_derivs.at(z).size())
			{
				outfile << ",\n ";
			}
		}

		if (z + 1 == zzb_derivs.size())
		{
			outfile << "\n";
		}
		else
		{
			outfile << ",\n ";
		}

		/*
		if (z + 1 == zzb_derivs.size() && !output_poles)
		{
			outfile << "}\n";
		}
		else
		{
			outfile << ",\n ";
		}*/
	}
}


void save_binary_convolved_block(boost::filesystem::ofstream & ofs,
	const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> & zzb_derivs_conv, const size_t binary_out_precision);


// this function change the convention to C1 convention by multiplying the following factor:
// (-1/2)^L Gamma[d-2+L] Gamma[(d-2)/2]/(Gamma[d-2] gamma[(d-2+2L)/2])
// for d=2, the factor should be treated as limit. The result is
// (-1/2)^L  2/(delta[L,0]+1)
Bigfloat get_normalization_factor(const Nu &nu, const int spin)
{
	if (nu.variant.type()==typeid(int64_t))
	{
		int64_t mu_int = boost::get<int64_t>(nu.variant);
		if (mu_int == 0)
		{
			using namespace boost::math;
			boost::multiprecision::mpfr_float rslt_mpfr = 1;
			rslt_mpfr = rslt_mpfr * pow(-1, spin) / pow(2, spin);
			if (spin > 0) rslt_mpfr = rslt_mpfr * 2;
			return boost::multiprecision::mpf_float(rslt_mpfr);
		}
	}

	//std::cout << "nu = " << nu.variant << "\n";
	//std::cout << "zeroQ(nu) = " << (nu.variant==0) << "\n";

	using namespace boost::math;
	boost::multiprecision::mpfr_float nu_f = boost::multiprecision::mpfr_float(nu.to_Bigfloat());
	boost::multiprecision::mpfr_float rslt_mpfr = tgamma(nu_f)*tgamma(spin + 2 * nu_f) / (tgamma(2 * nu_f)*tgamma(spin + nu_f));
	rslt_mpfr = rslt_mpfr * pow(-1, spin) / pow(2, spin);
	return boost::multiprecision::mpf_float(rslt_mpfr);
}

void write_spins(
  const boost::filesystem::path &nmax_dir, const Nu &nu,
  const std::string &dim, const std::string &Delta_12_string,
  const std::string &Delta_34_string, const std::set<int64_t> &spins,
  const Bigfloat &Delta_12, const Bigfloat &Delta_34, const int64_t &n_max,
  const bool &output_ab, const int64_t &kept_pole_order, const int64_t &order,
  const std::vector<Single_Spin> &shifted_blocks,
  const std::vector<std::vector<std::vector<Bigfloat>>> &user_poles,

  const bool &output_poles, const std::vector<std::string> & convolve_factor_str_list, const bool &nozzbDeriv, 
  const std::string & outfile_suffix, const size_t binary_out_precision,

  const size_t &num_threads, const size_t &thread_rank, const bool &debug, Timer &thread_timer)
{
  const Bigfloat nu_Bigfloat(nu.to_Bigfloat());

  auto spin(spins.begin());
  std::advance(spin, std::min(spins.size(), thread_rank));
  while(spin != spins.end())
    {
      const int64_t L(*spin);

	  if (binary_out_precision != 0) // output binary convolved block
	  {
		  std::vector<boost::math::tools::polynomial<Bigfloat>> a_derivs(2 * n_max,
			  { 0 });
		  for (size_t n = 0; n < a_derivs.size(); ++n)
		  {
			  std::vector<Bigfloat> coefficients(a_deriv_coefficients(n));

			  boost::math::tools::polynomial<Bigfloat> &a_deriv(a_derivs.at(n));
			  for (size_t r_deriv = 0; r_deriv < coefficients.size(); ++r_deriv)
			  {
				  a_deriv += shifted_blocks.at(L).numerator.at(r_deriv)
					  * coefficients.at(r_deriv);
			  }
		  }
		  const boost::math::tools::polynomial<Bigfloat> x({ 0, 1 });
		  const boost::math::tools::polynomial<Bigfloat> lambda(
			  L * (2 * nu_Bigfloat + L) + (-2 + L + x) * (2 * nu_Bigfloat + L + x));

		  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
			  ab_derivs(compute_ab_derivs(nu_Bigfloat, Delta_12, Delta_34, n_max,
				  lambda, a_derivs));

		  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
			  zzb_derivs(compute_zzb_derivs(ab_derivs, n_max));

		  for (auto& conv_factor_str : convolve_factor_str_list)
		  {
			  std::vector<std::string> val_pair;
			  boost::split(val_pair, conv_factor_str, boost::is_any_of("="));
			  if (val_pair.size() != 2)
			  {
				  std::cout << "unrecognizable convolve factor : " << conv_factor_str << "\n";
				  exit(1);
			  }

			  Bigfloat conv_factor(val_pair[1]);

			  Bigfloat norm_factor = get_normalization_factor(nu, (int)L);

			  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
				  zzb_derivs_conv(compute_convolve_zzb_derivs(zzb_derivs, conv_factor, norm_factor));

			  std::stringstream ss;
			  ss << "/" << val_pair[0] << "-L" << L << ".block";
			  boost::filesystem::ofstream outfile(nmax_dir / ss.str());

			  save_binary_convolved_block(outfile, zzb_derivs_conv, binary_out_precision);
		  }


		  for (size_t n = 0; n < num_threads && spin != spins.end(); ++n)
			  ++spin;
		  continue;
	  }



	  std::stringstream ss;
	  if (outfile_suffix.size()==0)
	  {
		  if (!output_ab)
			  ss << "/zzbDerivTable-d";
		  else
			  ss << "/abDerivTable-d";

		  ss << dim << "-delta12-" << Delta_12_string << "-delta34-"
			  << Delta_34_string << "-L" << L << "-nmax" << n_max
			  << "-keptPoleOrder" << kept_pole_order << "-order" << order << ".m";
	  }
	  else
	  {
		  ss << "/" << outfile_suffix << "-L" << L << ".m";
	  }

      boost::filesystem::ofstream outfile(nmax_dir / ss.str());
      outfile << std::fixed;
      if(debug)
        {
          std::cout << "Writing: " << (nmax_dir / ss.str()).string() << "\n";
        }
      outfile.precision(boost::multiprecision::mpf_float::default_precision());
      outfile << "{";

      std::vector<boost::math::tools::polynomial<Bigfloat>> a_derivs(2 * n_max,
                                                                     {0});
      for(size_t n = 0; n < a_derivs.size(); ++n)
        {
          std::vector<Bigfloat> coefficients(a_deriv_coefficients(n));

          boost::math::tools::polynomial<Bigfloat> &a_deriv(a_derivs.at(n));
          for(size_t r_deriv = 0; r_deriv < coefficients.size(); ++r_deriv)
            {
              a_deriv += shifted_blocks.at(L).numerator.at(r_deriv)
                         * coefficients.at(r_deriv);
            }
        }
      const boost::math::tools::polynomial<Bigfloat> x({0, 1});
      const boost::math::tools::polynomial<Bigfloat> lambda(
        L * (2 * nu_Bigfloat + L) + (-2 + L + x) * (2 * nu_Bigfloat + L + x));

      std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
        ab_derivs(compute_ab_derivs(nu_Bigfloat, Delta_12, Delta_34, n_max,
                                    lambda, a_derivs));

      if(!output_ab)
        {
          std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
            zzb_derivs(compute_zzb_derivs(ab_derivs, n_max));

		  if (!nozzbDeriv)
		  {
			  write_derivs(zzb_derivs, "zzbDeriv", outfile);
			  
			  if(convolve_factor_str_list.size()>0)outfile << ",\n ";

		  }

		  bool conv_factor_str_header=true;
		  for (auto& conv_factor_str : convolve_factor_str_list)
		  {
			  if (conv_factor_str_header)
				  conv_factor_str_header = false;
			  else
				  outfile << ",\n ";

			  std::vector<std::string> val_pair;
			  boost::split(val_pair, conv_factor_str, boost::is_any_of("="));
			  if (val_pair.size()!=2)
			  {
				  std::cout << "unrecognizable convolve factor : " << conv_factor_str << "\n";
				  exit(1);
			  }

			  Bigfloat conv_factor(val_pair[1]);

			  Bigfloat norm_factor = get_normalization_factor(nu, (int)L);

			  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
				  zzb_derivs_conv(compute_convolve_zzb_derivs(zzb_derivs, conv_factor, norm_factor));

			  
			  //zzb_derivs_conv_mpfr = zzb_derivs_conv;
			  std::vector<std::vector<boost::math::tools::polynomial<Bigfloatr>>> zzb_derivs_conv_mpfr(zzb_derivs_conv.size());
			  for (size_t m = 0; m < zzb_derivs_conv.size(); ++m)
			  {
				  zzb_derivs_conv_mpfr[m].resize(zzb_derivs[m].size());
				  for (size_t n = 0; n < zzb_derivs_conv[m].size(); ++n)
				  {
					  zzb_derivs_conv_mpfr[m][n].data().assign(std::begin(zzb_derivs_conv[m][n].data()), std::end(zzb_derivs_conv[m][n].data()));
				  }
			  }

			  write_derivs(zzb_derivs_conv, "zzbDerivF[" + val_pair[0] + "]["+std::to_string(L)+"]", outfile);
		  }

		  if (!output_poles)
			  outfile << "}\n";
		  else
			  outfile << ",\n";
        }
      else
        {
          // We use max_degree, because the a,b derivatives contain
          // spurious leading x^n powers with coefficients that are pure
          // numerical error. Since we can compute the degree of the
          // polynomial, we can safely truncate them. In z,zb case this is not
          // an issue since the polynomials happen to have the correct degree.
          // This happens because at intermediate stages b derivatives produce
          // extra 2 powers of x, whereas in the end a and b derivatives both
          // produce only 1 power (since is a smooth function of b at b=0).
          // This leads to cancellations which result in these spurious leading
          // powers.
          //
          // Maybe we should clean these powers up at the stage where we
          // compute them...
          const int64_t max_degree([&]() {
            int64_t result(0);
            if(user_poles.empty())
              {
                auto poles = cb_poles(nu, L, kept_pole_order);
                for(size_t index(0); index != poles.size(); ++index)
                  {
                    result += (index + 1) * poles[index].size();
                  }
              }
            else
              {
                for(size_t index(0); index != user_poles.size(); ++index)
                  {
                    result += (index + 1) * user_poles[index].size();
                  }
              }
            return result;
          }());

          for(size_t b = 0; b < ab_derivs.size(); ++b)
            {
              for(size_t a = 0; a < ab_derivs.at(b).size(); ++a)
                {
                  outfile << "abDeriv[" << a << "," << b << "] -> ";

                  for(size_t power = 0; power < ab_derivs.at(b).at(a).size()
                                        && power <= max_degree + a + b;
                      ++power)
                    {
                      outfile << ab_derivs.at(b).at(a)[power];
                      if(power != 0)
                        {
                          outfile << "*x^" << power;
                        }
                      if(power + 1 != ab_derivs.at(b).at(a).size()
                         && power != max_degree + a + b)
                        {
                          outfile << "\n   + ";
                        }
                    }
                  if(a + 1 != ab_derivs.at(b).size())
                    {
                      outfile << ",\n ";
                    }
                }
              if(b + 1 == ab_derivs.size() && !output_poles)
                {
                  outfile << "}\n";
                }
              else
                {
                  outfile << ",\n ";
                }
            }
        }
      if(output_poles)
        {
          outfile << "shiftedPoles -> {";
          for(size_t degree(0);
              degree != shifted_blocks.at(L).denominators.size(); ++degree)
            {
              if(degree != 0)
                {
                  outfile << ",\n          ";
                }
              outfile << "{";
              for(size_t pole = 0;
                  pole < shifted_blocks.at(L).denominators[degree].size();
                  ++pole)
                {
                  if(pole != 0)
                    {
                      outfile << ",\n           ";
                    }
                  outfile << shifted_blocks.at(L).denominators[degree][pole];
                }
              outfile << "}";
            }
          outfile << "}}\n";
        }
      for(size_t n = 0; n < num_threads && spin != spins.end(); ++n)
        {
          ++spin;
        }
    }
  thread_timer.stop();
}
