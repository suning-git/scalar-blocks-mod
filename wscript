import os, subprocess

def options(opt):
    #opt.load(['compiler_cxx','gnu_dirs','cxx17','boost','gmp','trilinos','eigen','threads'])
    opt.load(['compiler_cxx','gnu_dirs','cxx17','boost','gmpxx','mpfr','elemental','trilinos','eigen','threads'])

def configure(conf):
    if not 'CXX' in os.environ or os.environ['CXX']=='g++' or os.environ['CXX']=='icpc':
        conf.environ['CXX']='mpicxx'

    #conf.load(['compiler_cxx','gnu_dirs','cxx17','boost','gmp','trilinos','eigen','threads'])
    conf.load(['compiler_cxx','gnu_dirs','cxx17','boost','gmpxx','mpfr','elemental','trilinos','eigen','threads'])

    conf.env.git_version=subprocess.check_output('git describe --dirty', universal_newlines=True, shell=True).rstrip()
    conf.load('clang_compilation_database')
    
def build(bld):
    default_flags=['-Wall', '-Wextra', '-O3', '-D SCALAR_BLOCKS_VERSION_STRING="' + bld.env.git_version + '"']
    #default_flags=['-Wall', '-Wextra', '-g', '-D SCALAR_BLOCKS_VERSION_STRING="' + bld.env.git_version + '"']
    #use_packages=['cxx14','boost','gmp','trilinos','eigen','threads']
    use_packages=['cxx14','boost','gmpxx','mpfr','elemental','trilinos','eigen','threads']

    # libblocks
    library_sources=['src/parse_spin_ranges.cxx',
                     'src/Nu.cxx',
                     'src/mn_deriv_set.cxx',
                     'src/compute_blocks/compute_blocks.cxx',
                     'src/compute_blocks/fill_result.cxx',
                     'src/compute_blocks/fill_result_even_dim.cxx',
                     'src/compute_blocks/restore_r_delta.cxx',
                     'src/compute_blocks/r_delta_matrices.cxx',
                     'src/compute_blocks/compute_h_infinity.cxx',
                     'src/compute_blocks/Residues/Residues/Residues.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues/append_residues.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues/h_dot.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues_even_dim/append_residues_even_dim.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues_even_dim/process_single_spin/process_single_spin.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues_even_dim/process_single_spin/h_value.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues_even_dim/process_single_spin/h_derivative.cxx',
                     'src/compute_blocks/Residues/Residues/append_residues_even_dim/process_single_spin/h_regularized.cxx']

    bld.stlib(source=library_sources,
              target='blocks',
              cxxflags=default_flags,
              use=use_packages
    )

    # scalar_blocks
    bld.program(source=['src/scalar_blocks/main.cxx',
                        'src/scalar_blocks/blockdataIO.cxx',
                        'src/scalar_blocks/parse_user_poles.cxx',
                        'src/scalar_blocks/write_zzb/write_zzb.cxx',
                        'src/scalar_blocks/write_zzb/cb_poles.cxx',
                        'src/scalar_blocks/write_zzb/write_spins/compute_ab_derivs.cxx',
                        'src/scalar_blocks/write_zzb/write_spins/a_deriv_coefficients/a_deriv_coefficients.cxx',
                        'src/scalar_blocks/write_zzb/write_spins/a_deriv_coefficients/alpha_series.cxx',
                        'src/scalar_blocks/write_zzb/write_spins/write_spins.cxx',
                        'src/scalar_blocks/write_zzb/write_spins/compute_zzb_derivs.cxx',
                        'src/scalar_blocks/write_zzb/shift_blocks/shift_blocks.cxx',
                        'src/scalar_blocks/write_zzb/shift_blocks/shift_fraction.cxx',
                        'src/scalar_blocks/write_zzb/shift_blocks/shift_poles.cxx',
                        'src/scalar_blocks/write_zzb/shift_blocks/together_with_factors.cxx'],
                target='scalar_blocks_mod',
                cxxflags=default_flags,
                use=use_packages + ['blocks']
    )
